import { Component } from '@angular/core';
import { BrowserTab } from '@ionic-native/browser-tab/ngx';
import { InAppBrowser} from '@ionic-native/in-app-browser/ngx';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
URL = 'http://www.aevaweb.com/'
  constructor(private browserTab: BrowserTab, private inAppBrowser: InAppBrowser) {}

  openBrowser(){
    this.browserTab.isAvailable()
    .then(isAvailable => {
      if (isAvailable) {
        this.browserTab.openUrl(this.URL);
      } else {
        // open URL with InAppBrowser instead or SafariViewController
      }
    });
  }
  openBrowser2(){
    this.inAppBrowser.create(this.URL).show()
  }

}
